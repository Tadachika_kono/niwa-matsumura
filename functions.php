<?php

// サイドバーのウィジェット
add_action( 'after_setup_theme', 'register_menu' );
function register_menu() {
  register_nav_menu( 'primary', __( 'Primary Menu', 'theme-slug' ) );
}

/** アイキャッチ画像サイズ */
add_theme_support( 'post-thumbnails' );
add_image_size( 'list_thumbnail', 1240, 780, true );
add_image_size( 'main_thumbnail', 1980, 1246, true );


/*【出力カスタマイズ】検索結果のタイトルをカスタマイズ */
function wp_search_title($search_title){
	if(is_search()){
	  $search_title = '「'.get_search_query().'」の検索結果';
	}
	return $search_title;
  }
  add_filter('wp_title','wp_search_title');


  // 投稿画面タグチェックボックス
function rudr_post_tags_meta_box_remove() {
	$id = 'tagsdiv-post_tag'; // you can find it in a page source code (Ctrl+U)
	$post_type = 'post'; // remove only from post edit screen
	$position = 'side';
	remove_meta_box( $id, $post_type, $position );
}
add_action( 'admin_menu', 'rudr_post_tags_meta_box_remove');
 
function rudr_add_new_tags_metabox(){
	$id = 'rudrtagsdiv-post_tag'; // it should be unique
	$heading = 'タグ一覧'; // meta box heading
	$callback = 'rudr_metabox_content'; // the name of the callback function
	$post_type = 'post';
	$position = 'side';
	$pri = 'default'; // priority, 'default' is good for us
	add_meta_box( $id, $heading, $callback, $post_type, $position, $pri );
}
add_action( 'admin_menu', 'rudr_add_new_tags_metabox');
 
function rudr_metabox_content($post) {  
 
	// get all blog post tags as an array of objects
	$all_tags = get_terms( array('taxonomy' => 'post_tag', 'hide_empty' => 0) ); 
 
	// get all tags assigned to a post
	$all_tags_of_post = get_the_terms( $post->ID, 'post_tag' );  
 
	// create an array of post tags ids
	$ids = array();
	if ( $all_tags_of_post ) {
		foreach ($all_tags_of_post as $tag ) {
			$ids[] = $tag->term_id;
		}
	}
 
	// HTML
	echo '<div id="taxonomy-post_tag" class="categorydiv">';
	echo '<input type="hidden" name="tax_input[post_tag][]" value="0" />';
	echo '<ul>';
	foreach( $all_tags as $tag ){
		// unchecked by default
		$checked = "";
		// if an ID of a tag in the loop is in the array of assigned post tags - then check the checkbox
		if ( in_array( $tag->term_id, $ids ) ) {
			$checked = " checked='checked'";
		}
		$id = 'post_tag-' . $tag->term_id;
		echo "<li id='{$id}'>";
		echo "<label><input type='checkbox' name='tax_input[post_tag][]' id='in-$id'". $checked ." value='$tag->slug' /> $tag->name</label><br />";
		echo "</li>";
	}
	echo '</ul></div>'; // end HTML
}

/** 管理画面内記事一覧でタグで絞り込む機能 */
function add_post_tag_filter() {
	global $post_type;
	if ( $post_type == 'post' ) {
	wp_dropdown_categories( array(
	'show_option_all' => 'タグ一覧',
	'orderby' => 'name',
	'hide_empty' => 0,
	'selected' => get_query_var( 'tag' ),
	'name' => 'tag',
	'taxonomy' => 'post_tag',
	'value_field' => 'slug',
	) );
	}
	}
	add_action( 'restrict_manage_posts', 'add_post_tag_filter' );
	function reset_post_tag_filter() {
	if ( isset( $_GET['tag'] ) && '0' == $_GET['tag'] ) {
	unset( $_GET['tag'] );
	}
	}
	add_action( 'load-edit.php', 'reset_post_tag_filter' );


/** カテゴリページのタイトルから "カテゴリ:"の接頭語を削除する */
add_filter( 'get_the_archive_title', function ($title) {
    if ( is_category() ) {
        $title = single_cat_title( '', false );
    } elseif ( is_tag() ) {
        $title = single_tag_title( '', false );
    } elseif ( is_author() ) {
        $title = '<span class="vcard">' . get_the_author() . '</span>' ;
    }
    return $title;
});

/** ぱんくず */
function get_categories_tree() {

    $post_categories = get_the_category();
    $cat_trees = array();
    $cat_counts = array();
    $cat_depth_max = 10;

    foreach ( $post_categories as $post_category ) {
        $depth = 0;
        $cat_IDs = array($post_category->cat_ID);
        $cat_obj = $post_category;

        while ( $depth < $cat_depth_max ) {
            if ( $cat_obj->category_parent == 0 ) {
                break;
            }
            $cat_obj = get_category($cat_obj->category_parent);
            array_unshift($cat_IDs, $cat_obj->cat_ID);
            $depth++;
        }
        array_push($cat_trees, $cat_IDs);
        array_push($cat_counts, count($cat_IDs));
    }

    $depth_max = max($cat_counts);
    $cat_key = array_search($depth_max, $cat_counts);
    $cat_tree = $cat_trees[$cat_key];
    return $cat_tree;
}

/** 検索結果から固定ページを除外する */

function SearchFilter($query) {
	if ( !is_admin() && $query->is_main_query() && $query->is_search() ) {
	$query->set( 'post_type', 'post' );
	}
	}
	add_action( 'pre_get_posts','SearchFilter' );


?>