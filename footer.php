<footer id="footer">
<span class="pagetop"><a href="#top" >&#8593;</a></span>
    <div class="site-info-top">	
		<div class="corporate-logo-footer">
			<div class="inner-corporate-logo-footer">
        		<a href="<?php echo home_url( '/' ); ?>"><img src="<?php echo get_stylesheet_directory_uri() ?>/images/common/main_logo.svg" alt="logo"></a>
    		</div>
		</div>
		<div class="sns-logos">
			<div class="instapinevillage">
				<a href="https://www.instagram.com/matsumura_teien/" target="_blank"><img src="<?php echo get_stylesheet_directory_uri() ?>/images/common/ico_insta.svg" alt="sns-icon"></a>
			</div>
			<div class="facebookpinevillage">
				<a href="https://www.facebook.com/niwa.matsumura/" target="_blank"><img src="<?php echo get_stylesheet_directory_uri() ?>/images/common/ico_face.svg" alt="sns-icon"></a>
			</div>
		</div>
	</div>
	<div class="site-info">
        COPYRIGHT <?php bloginfo( 'name' ); ?> ALL RIGHTS RESERVED.
    </div>
</footer>

</div><!-- #container -->
</div><!-- .wrap -->
<?php wp_footer(); ?>
</body>
</html>
