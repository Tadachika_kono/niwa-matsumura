<?php get_header();?>
<main id="main" class="<?php echo get_post($wp_query->post->ID)->post_name; ?>">
	<article class="page">
		<section class="inner">
				<?php
				while ( have_posts() ) : the_post();
					get_template_part( 'template-parts/content', 'page' );
					// If comments are open or we have at least one comment, load up the comment template.
					if ( comments_open() || get_comments_number() ) :
						comments_template();
					endif;
				endwhile; // End of the loop.
				?>
		</section>
	</article>
</main>
<?php get_footer();?>
