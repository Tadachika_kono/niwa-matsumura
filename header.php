<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="format-detection" content="telephone=no">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="stylesheet" href="https://fonts.googleapis.com/earlyaccess/sawarabimincho.css">
<link rel="icon" href="<?php echo get_stylesheet_directory_uri() ?>/images/common/favicon.png">
<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri() ?>/style.css">
<?php wp_head(); ?>
<script src="//ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<!-- ページ読み込みローディング画面 -->
<script type="text/javascript">
$(function(){
   $(window).on('load',function(){
     $("#loader-bg").delay(1000).fadeOut('slow');
   });
   function loaderClose(){
     $("#loader-bg").fadeOut('slow');
   }
   setTimeout(loaderClose,2000);
});
</script>
<!-- アコーディオンメニュー -->
<script type="text/javascript">
    jQuery(function($) {
    $('.nav-button').on('click',function(){
        if( $(this).hasClass('active') ){
        $(this).removeClass('active');
        $('.nav-wrap').addClass('close').removeClass('open');
        }else {
        $(this).addClass('active');
        $('.nav-wrap').addClass('open').removeClass('close'); 
        }
    });
    });
</script>
<!-- トップへ戻るボタン -->
<script>
    $(function() {
    var topBtn = $('.pagetop');    
    topBtn.hide();
    //スクロールが200に達したらボタン表示
    $(window).scroll(function () {
        if ($(this).scrollTop() > 200) {
            topBtn.fadeIn();
        } else {
            topBtn.fadeOut();
        }
    });
    //スクロールしてトップ
    topBtn.click(function () {
        $('body,html').animate({
            scrollTop: 0
        }, 500);
        return false;
    });
});
</script>

<!-- お知らせ・FACEBOOK埋め込み -->

<script>
$(function() {
    // ページプラグインの埋め込みコードを返す
    function pagePluginCode(w) {

        // 幅に応じて高さを変更する場合は、変数 h に高さの値を代入
        if(w > 400) {
            var h = 630;
        } else {
            var h = 500;
        }

        // 書き換えたStep3のコードを「return」に記述
        return '<div class="fb-page" data-href="https://www.facebook.com/niwa.matsumura/" data-tabs="timeline" data-width="' + w + '" data-height="' + h + '" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="false"><blockquote cite="https://www.facebook.com/moreplusdesign.tokamachi/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/moreplusdesign.tokamachi/">More⁺Design（モアプラスデザイン）</a></blockquote></div>';
    }
 
    // ページプラグインを追加する要素
    var facebookWrap = $('.facebook-wrapper');
    var fbBeforeWidth = ''; // 前回変更したときの幅
    var fbWidth = facebookWrap.width(); // 今回変更する幅
    var fbTimer = false;
    $(window).on('load resize', function() {
        if (fbTimer !== false) {
            clearTimeout(fbTimer);
        }
        fbTimer = setTimeout(function() {
            fbWidth = Math.floor(facebookWrap.width()); // 変更後の幅を取得し、小数点以下切り捨て
            // 前回の幅から変更があった場合のみ処理
            if(fbWidth != fbBeforeWidth) {
                facebookWrap.html(pagePluginCode(fbWidth)); // ページプラグインのコード変更
                window.FB.XFBML.parse(); // ページプラグインの再読み込み
                fbBeforeWidth = fbWidth; // 今回変更分を保存
            }
        }, 200);
    });
});
</script>

<!-- Adobe font 
<script>
  (function(d) {
    var config = {
      kitId: 'wvh4rvn',
      scriptTimeout: 3000,
      async: true
    },
    h=d.documentElement,t=setTimeout(function(){h.className=h.className.replace(/\bwf-loading\b/g,"")+" wf-inactive";},config.scriptTimeout),tk=d.createElement("script"),f=false,s=d.getElementsByTagName("script")[0],a;h.className+=" wf-loading";tk.src='https://use.typekit.net/'+config.kitId+'.js';tk.async=true;tk.onload=tk.onreadystatechange=function(){a=this.readyState;if(f||a&&a!="complete"&&a!="loaded")return;f=true;clearTimeout(t);try{Typekit.load(config)}catch(e){}};s.parentNode.insertBefore(tk,s)
  })(document);
</script>-->

<script>
  (function(d) {
    var config = {
      kitId: 'sqz7rqu',
      scriptTimeout: 3000,
      async: true
    },
    h=d.documentElement,t=setTimeout(function(){h.className=h.className.replace(/\bwf-loading\b/g,"")+" wf-inactive";},config.scriptTimeout),tk=d.createElement("script"),f=false,s=d.getElementsByTagName("script")[0],a;h.className+=" wf-loading";tk.src='https://use.typekit.net/'+config.kitId+'.js';tk.async=true;tk.onload=tk.onreadystatechange=function(){a=this.readyState;if(f||a&&a!="complete"&&a!="loaded")return;f=true;clearTimeout(t);try{Typekit.load(config)}catch(e){}};s.parentNode.insertBefore(tk,s)
  })(document);
</script>

<!-- PC or SP 読み込み分岐 -->
<?php
$ua=$_SERVER['HTTP_USER_AGENT'];
$browser=((strpos($ua, 'iPhone')!==false)||(strpos($ua, 'iPod')!==false)||(strpos($ua, 'Android')!==false));
?>
<?php if ($browser=='sp') { ?>
    <!-- sp -->
<?php } else { ?>
    <!-- pc -->
<?php } ?>
</head>

<body <?php body_class(); ?>>
<div id="loader-bg">
<div id="loader">
    <img src="<?php echo get_stylesheet_directory_uri() ?>/images/common/icon_loader.gif" />
</div>
</div>

<div id="wrap" >
<div id="container">

<?php if ( is_home() || is_front_page() ) : ?>
<header id="header_top">
<?php else : ?>
<header id="header">
<?php endif; ?>

<div class="header_inner">

    <div class="navigation">
        <h1 class="logo"><a href="<?php echo home_url(); ?>">(有)松村庭園設計</a></h1>
        <a class="nav-button" href="#">
        <span></span>
        <span></span>
        <span></span>
        </a>
        <nav class="nav-wrap">
            <?php wp_nav_menu(); ?>
        </nav>
    </div>
    <?php if ( is_home() || is_front_page() ) : ?>
        <div class="top_mainvisual_phrase">
        <img src="<?php echo get_stylesheet_directory_uri() ?>/images/common/top_mainvisual_phrase.svg" alt="toppage-mainvisual-phrase" class="toppage_mainvisual_phrase_niwa">
        <img src="<?php echo get_stylesheet_directory_uri() ?>/images/common/top_mainvisual_phrase_sp-01.svg" alt="toppage-mainvisual-phrase" class="toppage_mainvisual_phrase_niwa_sp">
    </div>
<?php else : ?>

<?php endif; ?>

</div>
</header>